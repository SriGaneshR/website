const canvas = document.getElementById('canvas1')
const ctx = canvas.getContext('2d')
canvas.width = 300 //window.innerWidth
canvas.height = 100 //window.innerHeight

let particleArray = []

let offsetX = 1
let offsetY = 1

let particleSpacing = 5
let textX = 0
let textY = 0

let particleColor = 'rgb(235,219,178)'
let particleColor2 = 'rgb(215, 153, 33)'
let connectorColor = 'rgb(215, 153, 33)'

let textToDisplay = 'Alpha'

const mouse = {
    x: null,
    y: null,
    radius: 25
}

window.addEventListener('mousemove', (e) => {
    mouse.x = e.x
    mouse.y = e.y
})

//ctx.textAlign = 'center'
ctx.textBaseline = 'top'
ctx.fillStyle = particleColor
ctx.font = '16px custom'
ctx.fillText(textToDisplay, textX, textY)

const textCoordinates = ctx.getImageData(0, 0, canvas.width, canvas.height)

class Particle {
    constructor(x, y) {
        this.x = x
        this.y = y
        this.size = 1
        this.baseX = this.x
        this.baseY = this.y
        this.density = (Math.random() * 30) + 1
        this.color = particleColor
    }
    draw() {
        ctx.fillStyle = this.color
        ctx.beginPath()
        ctx.arc(this.x, this.y, this.size, 0, Math.PI * 2)
        ctx.closePath()
        ctx.fill()
    }
    update() {
        let dx = mouse.x - this.x
        let dy = mouse.y - this.y
        let distance = Math.hypot(dx, dy)
        let forceDirectionX = dx/distance
        let forceDirectionY = dy/distance
        let maxDistance = mouse.radius
        let force = (maxDistance - distance) / maxDistance
        let directionX = forceDirectionX * force * this.density
        let directionY = forceDirectionY * force * this.density
        if (distance < maxDistance) {
            this.x -= directionX
            this.y -= directionY
            if (distance > maxDistance * 0.5) {
                this.color = particleColor2
            }
        } else {
            if (this.x !== this.baseX) {
                dx = this.x - this.baseX
                this.x -= dx / 20
            }
            if (this.y !== this.baseY) {
                dy = this.y - this.baseY
                this.y -= dy / 20
            }
            this.color = particleColor
        }
    }
}

function init() {
    particleArray = []
    for (let y = 0, y2 = textCoordinates.height; y < y2; y++) {
        for (let x = 0, x2 = textCoordinates.width; x < x2; x++) {
            if (textCoordinates.data[y * 4 * textCoordinates.width + x * 4 + 3] > 128) {
                let positionX = x + offsetX
                let positionY = y + offsetY
                particleArray.push(new Particle(positionX * particleSpacing, positionY * particleSpacing))
            }
        }
    }
}
init()


function animate() {
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    for (let i = 0; i < particleArray.length; i++) {
        particleArray[i].draw()
        particleArray[i].update()
    }
    connect()
    requestAnimationFrame(animate)
}
animate()


function connect() {
    let opacity = 1
    for (let a = 0; a < particleArray.length; a++) {
        for (let b = a; b < particleArray.length; b++) {
            let dx = particleArray[a].x - particleArray[b].x
            let dy = particleArray[a].y - particleArray[b].y
            let distance = Math.hypot(dx, dy)

            let maxConnectDistance = 9

            if (distance < maxConnectDistance) {
                opacity = 1 - distance / maxConnectDistance
                ctx.strokeStyle = `rgba(168,153,132,${opacity})`
                if (distance > maxConnectDistance - 1) {
                    ctx.strokeStyle = 'yellow'
                }
                ctx.lineWidth = 1
                ctx.beginPath()
                ctx.moveTo(particleArray[a].x, particleArray[a].y)
                ctx.lineTo(particleArray[b].x, particleArray[b].y)
                ctx.stroke()
                ctx.closePath()
            }
        }
    }
}
