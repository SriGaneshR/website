const canvas = document.getElementById('canvas1')
const context = canvas.getContext('2d')
canvas.width = window.innerWidth
canvas.height = window.innerHeight

const canvas2 = document.getElementById('canvas2')
const context2 = canvas2.getContext('2d')
canvas2.width = window.innerWidth
canvas2.height = window.innerHeight

context.fillStyle = 'white'
context.strokeStyle = 'white'
context.lineWidth = 1

// gradient
const gradient = context.createLinearGradient(0, 0, canvas.width, canvas.height)
gradient.addColorStop(0, 'white')
gradient.addColorStop(0.5, 'magenta')
gradient.addColorStop(1, 'blue')


class Particle {
    constructor(effect) {
        this.effect = effect
        this.radius = Math.floor(20 * Math.random() + 10)
        this.ballRadius = this.radius + 0.5 * context.lineWidth

        // the particles will not be generated beyond the canvas
        this.x = this.ballRadius + Math.random() * (this.effect.width - 2*this.ballRadius)
        this.y = this.ballRadius + Math.random() * (this.effect.height - 2*this.ballRadius)

        this.vx = Math.random() - 1 // x-axis velocity
        this.vy = Math.random() - 1 // y-axis velocity

        this.pushX = 0
        this.pushY = 0
        this.friction = 0.95
    }
    draw(context) {
        context.fillStyle = 'white'
        //context.fillStyle = gradient
        context.beginPath()
        context.arc(this.x, this.y, this.radius, 0, 2*Math.PI)
        context.fill()
        // context.stroke()
    }
    update() {
        if (this.effect.mouse.pressed) {
            const dx = this.x - this.effect.mouse.x
            const dy = this.y - this.effect.mouse.y
            const distance = Math.hypot(dx, dy)
            const force = distance / this.effect.mouse.radius 
            if (distance < this.effect.mouse.radius) {
                const angle = Math.atan2(dy, dx)
                this.pushX -= Math.cos(angle) * force
                this.pushY -= Math.sin(angle) * force
            }
        }

        this.x += this.vx + (this.pushX *= this.friction)
        this.y += this.vy + (this.pushY *= this.friction)

        if (this.x < this.ballRadius) {
            this.x = this.ballRadius
            this.vx *= -1
        } else if (this.x > this.effect.width - this.ballRadius) {
            this.x = this.effect.width - this.ballRadius
            this.vx *= -1
        }
        if (this.y < this.ballRadius) {
            this.y = this.ballRadius
            this.vy *= -1
        } else if (this.y > this.effect.height - this.ballRadius) {
            this.y = this.effect.height - this.ballRadius
            this.vy *= -1
        }
    }
}

class Effect {
    constructor(canvas) {
        this.canvas = canvas
        this.width = this.canvas.width
        this.height = this.canvas.height
        this.particles = []
        this.numberOfParticles = 100
        this.createParticles()

        this.mouse = {
            x: 0,
            y: 0,
            pressed: false,
            radius: 150,
        }

        window.addEventListener('resize', (e) => {
            this.resize(e.target.window.innerWidth, e.target.window.innerHeight)
        })
        window.addEventListener('mousemove', (e) => {
            if (this.mouse.pressed) {
                this.mouse.x = e.x
                this.mouse.y = e.y
            }
        })
        window.addEventListener('mousedown', (e) => {
            this.mouse.pressed = true
            this.mouse.x = e.x
            this.mouse.y = e.y
        })
        window.addEventListener('mouseup', (e) => {
            this.mouse.pressed = false
        })
    }
    createParticles() {
        for (let i = 0; i < this.numberOfParticles; i++) {
            this.particles.push(new Particle(this))
        }
    }
    handleParticles(context, context2) {
        this.connectParticles(context2)
        this.particles.forEach((particle) => {
            particle.draw(context)
            particle.update()
        })
    }
    connectParticles(context) {
        const maxDistance = 100
        for (let a = 0; a < this.particles.length; a++) {
            for (let b = a; b < this.particles.length; b++) {
                const dx = this.particles[a].x - this.particles[b].x
                const dy = this.particles[a].y - this.particles[b].y
                const distance = Math.hypot(dx, dy)
                if (distance <= maxDistance) {
                    context.save() // save context properties until now
                    const opacity = 1 - distance/maxDistance
                    context.strokeStyle = `rgba(0,0,0,${opacity})`
                    context.beginPath()
                    context.moveTo(this.particles[a].x, this.particles[a].y)
                    context.lineTo(this.particles[b].x, this.particles[b].y)
                    context.stroke()
                    context.restore() // restore saved properties
                }
            }
        }
    }
    resize(width, height) {
        this.canvas.width = width
        this.canvas.height = height
        this.width = width
        this.height = height
    }
}

const effect = new Effect(canvas)

function animate() {
    context.clearRect(0, 0, canvas.width, canvas.height)
    context2.clearRect(0, 0, canvas.width, canvas.height)
    effect.handleParticles(context, context2)
    requestAnimationFrame(animate)
}
animate()
