window.addEventListener('load', function() {
    const canvas = document.getElementById('canvas1')
    const ctx = canvas.getContext('2d', {
        willReadFrequently: true
    })
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight


    class Particle {
        constructor(effect, x, y, color) {
            this.effect = effect
            this.x = Math.random() * this.effect.canvasWidth
            this.y = Math.random() * this.effect.canvasHeight
            // this.x = this.effect.canvasWidth/2
            // this.y = this.effect.canvasHeight/2
            this.color = color
            this.originX = x
            this.originY = y
            this.size = this.effect.gap
            this.dx = 0
            this.dy = 0
            this.vx = 0
            this.vy = 0
            this.force = 0
            this.angle = 0
            this.distance = 0
            //this.friction = 0.6
            //this.ease = 0.1
            this.friction = Math.random() * 0.6 + 0.15
            this.ease = Math.random() * 0.1 + 0.005
        }
        draw() {
            this.effect.context.fillStyle = this.color
            this.effect.context.strokeStyle = 'white'
            // this.effect.context.fillRect(this.x, this.y, this.size, this.size)
            this.effect.context.fillRect(this.x, this.y, this.size/2, this.size/2)
            // this.effect.context.arc(this.x, this.y, this.size*2, 0, Math.PI*2)
        }
        update() {
            this.dx = this.effect.mouse.x - this.x
            this.dy = this.effect.mouse.y - this.y
            this.distance = this.dx * this.dx + this.dy * this.dy
            this.force = -this.effect.mouse.radius / this.distance

            if (this.distance < this.effect.mouse.radius) {
                this.angle = Math.atan2(this.dy, this.dx)
                this.vx += this.force * Math.cos(this.angle)
                this.vy += this.force * Math.sin(this.angle)
            }
            this.x += (this.vx *= this.friction) + (this.originX - this.x) * this.ease
            this.y += (this.vy *= this.friction) + (this.originY - this.y) * this.ease
        }
    }

    class Effect {
        constructor(context, canvasWidth, canvasHeight) {
            this.context = context
            this.canvasHeight = canvasHeight
            this.canvasWidth = canvasWidth
            this.textX = canvasWidth/2
            this.textY = canvasHeight/2
            this.fontSize = 40
            this.lineHeight = this.fontSize + 1
            this.maxTextWidth = this.canvasWidth * 0.8
            this.verticalOffset = 0
            this.textInput = document.getElementById('textInput')
            this.textInput.addEventListener('keyup', (e) => {
                if (e.key !== ' ') {
                    this.context.clearRect(0, 0, this.canvasWidth, this.canvasHeight)
                    this.wrapText(e.target.value)
                }
            })

            // particle text
            this.particle = []
            this.gap = 1;
            this.mouse = {
                radius: 2000,
                x: 0,
                y: 0
            }
            window.addEventListener('mousemove', (e) => {
                this.mouse.x = e.x
                this.mouse.y = e.y
            })
        }
        wrapText(text) {
            const gradient = this.context.createLinearGradient(0,0,canvas.width, canvas.height)
            gradient.addColorStop(0.3, 'red')
            gradient.addColorStop(0.5, 'fuchsia')
            gradient.addColorStop(0.7, 'purple')
            // this.context.fillStyle = gradient
            this.context.fillStyle = 'yellow'
            this.context.font = this.fontSize + 'px ubuntu'

            // this.context.letterSpacing = '3px' // experimental
            this.context.textAlign = 'center' // default: 'start'
            this.context.textBaseline = 'middle' // top, middle, bottom

            this.context.lineWidth = 3;
            // this.context.strokeStyle = 'blue'

            // break multiline text
            let linesArray = []
            let linesCounter = 0;
            let line = ''
            let words = text.split(' ')
            for (let i = 0; i < words.length; i++) {
                let testLine = line + words[i] + ' '
                if (this.context.measureText(testLine).width > this.maxTextWidth) {
                    line = words[i] + ' '
                    linesCounter += 1
                } else {
                    line = testLine
                }
                linesArray[linesCounter] = line
            }
            let textHeight = this.lineHeight * linesCounter
            this.textY = this.canvasHeight/2 - textHeight/2 + this.verticalOffset

            linesArray.forEach((e1, index) => {
                this.context.fillText(e1, this.textX, this.textY + (index * this.lineHeight))
                // this.context.strokeText(e1, this.textX, this.textY + (index * this.lineHeight))
            })
        this.convertToParticles()
        }

        convertToParticles() {
            this.particles = []
            const pixels = this.context.getImageData(0,0,this.canvasWidth, this.canvasHeight).data
            this.context.clearRect(0,0,this.canvasWidth, this.canvasHeight)
            for (let y = 0; y < this.canvasHeight; y += this.gap) {
                for (let x = 0; x < this.canvasWidth; x += this.gap) {
                    const index = (y * this.canvasWidth + x) * 4
                    const alpha = pixels[index + 3]
                    if (alpha > 0) {
                        const red = pixels[index]
                        const green = pixels[index+1]
                        const blue = pixels[index+2]
                        const color = `rgb(${red},${green},${blue})`
                        this.particles.push(new Particle(this, x, y, color))
                    }
                }
            }
        }
        render(context) {
            // this.connectParticles(context)
            this.particles.forEach((particle) => {
                particle.update()
                particle.draw()
            })
        }
        resize(width, height) {
            this.canvasWidth = width
            this.canvasHeight = height
            this.textX = this.canvasWidth/2
            this.textY = this.canvasHeight/2
            this.maxTextWidth = this.canvasWidth * 0.8
        }
        connectParticles(context) {
            const maxDistance = 20;
            for (let a = 0; a < this.particles.length; a++) {
                for (let b = a; b < this.particles.length; b++) {
                    const deltaX = this.particles[a].x - this.particles[b].x
                    const deltaY = this.particles[a].y - this.particles[b].y
                    const particleDistance = Math.hypot(deltaX, deltaY)
                    if (particleDistance < maxDistance) {
                        this.context.save()
                        const opacity = 1 - (particleDistance/maxDistance) *0.8
                        this.context.globalAlpha = opacity
                        this.context.beginPath()
                        this.context.moveTo(this.particles[a].x, this.particles[a].y)
                        this.context.lineTo(this.particles[b].x, this.particles[b].y)
                        this.context.stroke()
                        this.context.restore()
                    }
                }
            }
        }
    }

    const effect = new Effect(ctx, canvas.width, canvas.height)
    effect.wrapText(effect.textInput.value)
    effect.render(this.context)

    function animate() {
        ctx.clearRect(0,0,canvas.width, canvas.height)
        effect.render(this.context)
        requestAnimationFrame(animate)
    }
    animate()
    
    window.addEventListener('resize', function() {
        canvas.width = window.innerWidth
        canvas.height = window.innerHeight
        effect.resize(canvas.width, canvas.height)
        effect.wrapText(effect.textInput.value)
    })
})