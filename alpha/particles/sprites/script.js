class Particle {
    constructor(effect) {
        this.effect = effect

        // sprite sheets
        this.image = document.getElementById('stars_sprite') // 150px / 150px
        this.spriteWidth = 50 // hardcoded for now
        this.spriteHeight = 50 // hardcoded for now
        this.sizeModifier = 0.5 + Math.random() * 0.5
        this.width = this.spriteWidth * this.sizeModifier
        this.height = this.spriteHeight * this.sizeModifier
        this.halfWidth = 0.5 * this.width
        this.halfHeight = 0.5 * this.height
        this.frameX = Math.floor(Math.random() * 3) // 0 to 2
        this.frameY = Math.floor(Math.random() * 3) // 0 to 2


        this.radius = Math.floor(10 * Math.random() + 1)
        // this.ballRadius = this.radius + 0.5 * this.effect.context.lineWidth
        this.ballRadius = this.halfWidth

        // the particles will not be generated beyond the canvas
        this.x = this.ballRadius + Math.random() * (this.effect.width - 2*this.ballRadius)
        this.y = this.ballRadius + Math.random() * (this.effect.height - 2*this.ballRadius)

        this.vx = Math.random() - 1 // x-axis velocity
        this.vy = Math.random() - 1 // y-axis velocity

        this.pushX = 0
        this.pushY = 0
        this.friction = 0.95
    }
    draw(context) {
        // context.fillStyle = `hsl(${this.x + 0.5},100%,50%)`
        context.beginPath()
        // context.arc(this.x, this.y, this.radius, 0, 2*Math.PI)
        // context.fill()
        // context.stroke()
        
        // context.drawImage( ... )
        // 3 args: (image, posX, posY)
        // 5 args: (image, posX, posY, imgWidth, imgHeight)
        // 9 args (IMAGE, sourceX, soureY, sourceWidth. sourceHeight, posX, posY, imgWidth, imgHeight)
        context.drawImage(this.image, this.frameX * this.spriteWidth, this.frameY * this.spriteHeight, this.spriteWidth, this.spriteHeight, this.x - this.halfWidth, this.y - this.halfHeight, this.width, this.height)
    }
    update() {
        if (this.effect.mouse.pressed) {
            const dx = this.x - this.effect.mouse.x
            const dy = this.y - this.effect.mouse.y
            const distance = Math.hypot(dx, dy)
            const force = this.effect.mouse.radius / distance
            if (distance < this.effect.mouse.radius) {
                const angle = Math.atan2(dy, dx)
                this.pushX += Math.cos(angle)
                this.pushY += Math.sin(angle)
            }
        }

        this.x += this.vx + (this.pushX *= this.friction)
        this.y += this.vy + (this.pushY *= this.friction)

        if (this.x < this.ballRadius) {
            this.x = this.ballRadius
            this.vx *= -1
        } else if (this.x > this.effect.width - this.ballRadius) {
            this.x = this.effect.width - this.ballRadius
            this.vx *= -1
        }
        if (this.y < this.ballRadius) {
            this.y = this.ballRadius
            this.vy *= -1
        } else if (this.y > this.effect.height - this.ballRadius) {
            this.y = this.effect.height - this.ballRadius
            this.vy *= -1
        }
    }
}


class Effect {
    constructor(canvas, context) {
        this.canvas = canvas
        this.context = context
        this.width = this.canvas.width
        this.height = this.canvas.height
        this.particles = []
        this.numberOfParticles = 100
        this.createParticles()

        this.mouse = {
            x: 0,
            y: 0,
            pressed: false,
            radius: 150,
        }

        window.addEventListener('resize', (e) => {
            this.resize(e.target.window.innerWidth, e.target.window.innerHeight)
        })
        window.addEventListener('mousemove', (e) => {
            if (this.mouse.pressed) {
                this.mouse.x = e.x
                this.mouse.y = e.y
            }
        })
        window.addEventListener('mousedown', (e) => {
            this.mouse.pressed = true
            this.mouse.x = e.x
            this.mouse.y = e.y
        })
        window.addEventListener('mouseup', (e) => {
            this.mouse.pressed = false
        })
    }
    createParticles() {
        for (let i = 0; i < this.numberOfParticles; i++) {
            this.particles.push(new Particle(this))
        }
    }
    handleParticles(context) {
        // this.connectParticles(context)
        this.particles.forEach((particle) => {
            particle.draw(context)
            particle.update()
        })
    }
    connectParticles(context) {
        const maxDistance = 70
        for (let a = 0; a < this.particles.length; a++) {
            for (let b = a; b < this.particles.length; b++) {
                const dx = this.particles[a].x - this.particles[b].x
                const dy = this.particles[a].y - this.particles[b].y
                const distance = Math.hypot(dx, dy)
                if (distance <= maxDistance) {
                    context.save() // save context properties until now
                    const opacity = 1 - distance/maxDistance
                    context.strokeStyle = `rgba(255,255,255,${opacity})`
                    context.beginPath()
                    context.moveTo(this.particles[a].x, this.particles[a].y)
                    context.lineTo(this.particles[b].x, this.particles[b].y)
                    context.stroke()
                    context.restore() // restore saved properties
                }
            }
        }
    }
    resize(width, height) {
        this.canvas.width = width
        this.canvas.height = height
        this.width = width
        this.height = height
    }
}


window.addEventListener('load', () => {
    const canvas = document.getElementById('canvas1')
    const context = canvas.getContext('2d')
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight

    context.fillStyle = 'white'
    context.strokeStyle = 'white'
    context.lineWidth = 1

    const effect = new Effect(canvas, context)

    function animate() {
        context.clearRect(0, 0, canvas.width, canvas.height)
        effect.handleParticles(context)
        requestAnimationFrame(animate)
    }
    animate()
})
