const canvas = document.getElementById('canvas1')
const context = canvas.getContext('2d')
canvas.width = window.innerWidth
canvas.height = window.innerHeight

context.fillStyle = 'white'
context.strokeStyle = 'black'
context.lineWidth = 1

// gradient
const gradient = context.createLinearGradient(0, 0, canvas.width, canvas.height)
gradient.addColorStop(0, 'pink')
gradient.addColorStop(0.5, 'red')
gradient.addColorStop(1, 'magenta')


class Particle {
    constructor(effect) {
        this.effect = effect
        this.radius = Math.floor(8 * Math.random() + 8)
        this.ballRadius = this.radius + 0.5 * context.lineWidth

        this.minRadius = this.radius
        this.maxRadius = this.radius * 5

        // the particles will not be generated beyond the canvas
        this.x = this.ballRadius + Math.random() * (this.effect.width - 2*this.ballRadius)
        this.y = this.ballRadius + Math.random() * (this.effect.height - 2*this.ballRadius)

        this.vx = Math.random()*0.2 - 0.1 // x-axis velocity
        this.vy = Math.random()*0.2 - 0.1 // y-axis velocity

        this.friction = 0.95
    }
    draw(context) {
        //context.fillStyle = `hsl(${this.x + 0.5},100%,50%)`
        context.fillStyle = gradient
        context.beginPath()
        context.arc(this.x, this.y, this.radius, 0, 2*Math.PI)
        context.fill()
        context.stroke()
        
        context.fillStyle = 'white'
        context.beginPath()
        context.arc(this.x - this.radius * 0.2, this.y - this.radius * 0.3, this.radius * 0.6, 0, 2 * Math.PI)
        context.fill()
    }
    update() {
        if (this.effect.mouse.pressed) {
            const dx = this.x - this.effect.mouse.x
            const dy = this.y - this.effect.mouse.y
            const distance = Math.hypot(dx, dy)
            if (distance < this.effect.mouse.radius && this.radius < this.maxRadius) {
                this.radius += 2
            }
            if (this.radius > this.minRadius) {
                this.radius -= 0.1
            }
        }

        this.x += this.vx
        this.y += this.vy

        if (this.x < this.ballRadius) {
            this.x = this.ballRadius
            this.vx *= -1
        } else if (this.x > this.effect.width - this.ballRadius) {
            this.x = this.effect.width - this.ballRadius
            this.vx *= -1
        }
        if (this.y < this.ballRadius) {
            this.y = this.ballRadius
            this.vy *= -1
        } else if (this.y > this.effect.height - this.ballRadius) {
            this.y = this.effect.height - this.ballRadius
            this.vy *= -1
        }
    }
}

class Effect {
    constructor(canvas) {
        this.canvas = canvas
        this.width = this.canvas.width
        this.height = this.canvas.height
        this.particles = []
        this.numberOfParticles = 400
        this.createParticles()

        this.mouse = {
            x: 0,
            y: 0,
            pressed: false,
            radius: 50,
        }

        window.addEventListener('resize', (e) => {
            this.resize(e.target.window.innerWidth, e.target.window.innerHeight)
        })
        window.addEventListener('mousemove', (e) => {
            if (this.mouse.pressed) {
                this.mouse.x = e.x
                this.mouse.y = e.y
            }
        })
        window.addEventListener('mousedown', (e) => {
            this.mouse.pressed = true
            this.mouse.x = e.x
            this.mouse.y = e.y
        })
        window.addEventListener('mouseup', (e) => {
            this.mouse.pressed = false
        })
    }
    createParticles() {
        for (let i = 0; i < this.numberOfParticles; i++) {
            this.particles.push(new Particle(this))
        }
    }
    handleParticles(context) {
        //this.connectParticles(context)
        this.particles.forEach((particle) => {
            particle.draw(context)
            particle.update()
        })
    }
    connectParticles(context) {
        const maxDistance = 70
        for (let a = 0; a < this.particles.length; a++) {
            for (let b = a; b < this.particles.length; b++) {
                const dx = this.particles[a].x - this.particles[b].x
                const dy = this.particles[a].y - this.particles[b].y
                const distance = Math.hypot(dx, dy)
                if (distance <= maxDistance) {
                    context.save() // save context properties until now
                    const opacity = 1 - distance/maxDistance
                    context.strokeStyle = `rgba(0,0,0,${opacity})`
                    context.beginPath()
                    context.moveTo(this.particles[a].x, this.particles[a].y)
                    context.lineTo(this.particles[b].x, this.particles[b].y)
                    context.stroke()
                    context.restore() // restore saved properties
                }
            }
        }
    }
    resize(width, height) {
        this.canvas.width = width
        this.canvas.height = height
        this.width = width
        this.height = height
    }
}

const effect = new Effect(canvas)

function animate() {
    context.clearRect(0, 0, canvas.width, canvas.height)
    effect.handleParticles(context)
    requestAnimationFrame(animate)
}
animate()
