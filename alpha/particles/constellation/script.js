class Particle {
    constructor(effect) {
        this.effect = effect

        this.radius = Math.floor(7 * Math.random() + 3)
        this.x = Math.random() * (this.effect.canvas.width - 2 * this.radius) + this.radius
        this.y = Math.random() * (this.effect.canvas.height - 2 * this.radius) + this.radius
        this.vx = Math.random() * 0.9 + 0.1
        this.vy = Math.random() * 0.9 + 0.1

        this.ax = 0
        this.ay = 0
        this.friction = 0.95
    }
    draw(context) {
        context.fillStyle = `hsl(${360 * this.x / this.effect.canvas.width}, 100%, 50%)`
        context.beginPath()
        context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI)
        context.fill()
        context.closePath()
    }
    update() {
        if (this.effect.mouse.clicked) {
            const dx = this.x - this.effect.mouse.x
            const dy = this.y - this.effect.mouse.y
            const distance = Math.hypot(dx, dy)
            if (distance < this.effect.mouse.radius) {
                const force = this.effect.mouse.radius / distance
                const angle = Math.atan2(dy, dx)
                this.ax += Math.cos(angle) * force
                this.ay += Math.sin(angle) * force
            }
        }

        this.x += this.vx + (this.ax *= this.friction)
        this.y += this.vy + (this.ay *= this.friction)

        if (this.x < this.radius) {
            this.x = this.radius
            this.vx *= -1
        } else if (this.x > this.effect.canvas.width - this.radius) {
            this.x = this.effect.canvas.width - this.radius
            this.vx *= -1
        }
        if (this.y < this.radius) {
            this.y = this.radius
            this.vy *= -1
        } else if (this.y > this.effect.canvas.height - this.radius) {
            this.y = this.effect.canvas.height - this.radius
            this.vy *= -1
        }
    }
}

class Effect {
    constructor(canvas) {
        this.canvas = canvas
        this.particles = []
        this.numberOfParticles = 150
        this.createParticles()

        this.mouse = {
            x: null,
            y: null,
            clicked: false,
            radius: 100,
        }

        window.addEventListener('resize', (e) => {
            this.resize(e.target.window.innerWidth, e.target.window.innerHeight)
        })
        window.addEventListener('mousemove', (e) => {
            if (this.mouse.clicked) {
                this.mouse.x = e.x
                this.mouse.y = e.y
            }
        })
        window.addEventListener('mousedown', (e) => {
            this.mouse.clicked = true
            this.mouse.x = e.x
            this.mouse.y = e.y
        })
        window.addEventListener('mouseup', (e) => {
            this.mouse.clicked = false
        })
        window.addEventListener('touchstart', (e) => {
            this.mouse.clicked = true
            this.mouse.x = e.touches[0].clientX
            this.mouse.y = e.touches[0].clientY
        })
        window.addEventListener('touchmove', (e) => {
            if (this.mouse.clicked) {
                this.mouse.x = e.touches[0].clientX
                this.mouse.y = e.touches[0].clientY
            }
        })
        window.addEventListener('touchend', (e) => {
            this.mouse.clicked = false
        })
    }
    createParticles() {
        for (let i = 0; i < this.numberOfParticles; i++) {
            this.particles.push(new Particle(this))
        }
    }
    handleParticles(context) {
        this.connectParticles(context)
        this.particles.forEach(particle => {
            particle.draw(context)
            particle.update()
        })
    }
    connectParticles(context) {
        let maxDistance = 70
        for (let a = 0; a < this.numberOfParticles; a++) {
            for (let b = a; b < this.numberOfParticles; b++) {
                const dx = this.particles[a].x - this.particles[b].x
                const dy = this.particles[a].y - this.particles[b].y
                const distance = Math.hypot(dx, dy)
                
                if (distance <= maxDistance) {
                    let opacity = 1 - distance / maxDistance
                    context.save()
                    //context.globalAlpha = opacity
                    context.strokeStyle = `rgba(255,255,255,${opacity})`
                    context.beginPath()
                    context.moveTo(this.particles[a].x, this.particles[a].y)
                    context.lineTo(this.particles[b].x, this.particles[b].y)
                    context.stroke()
                    context.restore()
                }
            }
        }
    }
    resize(width, height) {
        this.canvas.width = width
        this.canvas.height = height
    }
}

window.addEventListener('load', () => {
    const canvas = document.getElementById('canvas1')
    const context = canvas.getContext('2d')

    canvas.width = window.innerWidth
    canvas.height = window.innerHeight

    context.fillStyle = 'white'
    context.strokeStyle = 'white'
    context.lineWidth = 1

    const effect = new Effect(canvas)

    function animate() {
        context.clearRect(0,0,canvas.width, canvas.height)
        effect.handleParticles(context)
        requestAnimationFrame(animate)
    }
    animate()
})
