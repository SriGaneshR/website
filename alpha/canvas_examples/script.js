let canvas
let context
let flowField
let flowFieldAnimation

window.onload = function() {
    canvas = document.getElementById('canvas1')
    context = canvas.getContext('2d')
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    flowField = new FlowFieldEffect(context, canvas.width, canvas.height)
    flowField.animate(0)
}

window.addEventListener('resize', () => {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    cancelAnimationFrame(flowFieldAnimation)
    flowField = new FlowFieldEffect(context, canvas.width, canvas.height)
    flowField.animate(0)
})

const mouse = {
    x: 0,
    y: 0,
}
window.addEventListener('mousemove', (e) => {
    mouse.x = e.x
    mouse.y = e.y
})

class FlowFieldEffect {
    // # -> private: cannot be accessed from outside
    #ctx
    #width
    #height
    constructor(context, width, height) {
        this.#ctx = context
        this.#width = width
        this.#height = height

        this.lastTime = 0
        this.fps = 30
        this.interval = 1000 / this.fps
        this.timer = 0

        this.#ctx.lineWidth = 1
        this.cellSize = 20
        this.radius = 5
        this.vr = 0.03

        this.gradient
        this.#createGradient()
        // this.#ctx.strokeStyle = 'white'
        this.#ctx.strokeStyle = this.gradient
    }
    #drawLine(angle, x, y) {
        let positionX = x
        let positionY = y
        let dx = mouse.x - positionX
        let dy = mouse.y - positionY
        let distance = Math.hypot(dx, dy)

        let length = distance*0.1
        this.#ctx.beginPath() // start drawing
        this.#ctx.moveTo(x, y) // goto (x,y)
        this.#ctx.lineTo(x + Math.cos(angle)*length, y + Math.sin(angle)*length)
        this.#ctx.stroke()
    }
    #createGradient() {
        this.gradient = this.#ctx.createLinearGradient(0, 0, this.#width, this.#height)
        this.gradient.addColorStop("0.1", "#ff5c33")
        this.gradient.addColorStop("0.2", "#ff66b3")
        this.gradient.addColorStop("0.4", "#ccccff")
        this.gradient.addColorStop("0.6", "#b3ffff")
        this.gradient.addColorStop("0.8", "#80ff80")
        this.gradient.addColorStop("0.9", "#fff333")
    }
    animate(timeStamp) {
        const deltaTime = timeStamp - this.lastTime
        this.lastTime = timeStamp
        if (this.timer > this.interval) {
            this.#ctx.clearRect(0, 0, this.#width, this.#height)
            this.radius += this.vr
            if (this.radius > 5 || this.radius < -5) {
                this.vr *= -1
            }

            for (let y = 0; y < this.#height; y += this.cellSize) {
                for (let x = 0; x < this.#width; x += this.cellSize) {
                    const angle = (Math.cos(x * .01) + Math.sin(y * .01)) * this.radius
                    this.#drawLine(angle, x, y)
                }
            }

            this.timer = 0
        } else {
            this.timer += deltaTime
        }

        flowFieldAnimation = requestAnimationFrame(this.animate.bind(this))
    }
}
