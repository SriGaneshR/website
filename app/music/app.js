//
// VARIABLES
//

const filesPage = document.getElementById('files-page');
const mainPage = document.getElementById('main-page');
const controlsPage = document.getElementById('controls-page');

const dropZone = document.getElementById('drop-zone');
const dropZoneMsg = document.querySelector('#drop-zone p');
const fileBrowser = document.getElementById('file-browser');
const musicList = document.getElementById('music-list');
const musicTitle = document.querySelectorAll('.music-title');
const music = document.getElementById('music-player');
const progressBar = document.getElementById('progress-bar');

const playButtons = document.getElementsByClassName('play-button');
const pauseButtons = document.getElementsByClassName('pause-button');


// Options
const optionsToggle = document.getElementById('options-toggle');
const optionsContainer = document.querySelector('.options-container');

// Autoplay and repeat
const toggleAutoplay = document.getElementById('toggle-autoplay');
const toggleRepeat = document.getElementById('toggle-repeat');
const repeatCounter = document.querySelector('.repeat-counter');
const repeatInc = document.getElementById('repeat-inc');
const repeatDec = document.getElementById('repeat-dec');
const repeatCounterDisplay = document.getElementById('repeat-count');

let files = [];
let file_names = [];
let total_count = 0;
let current_track_number = 0;
let title = "";
let track_name = "";
let progressValue = 0;

const dropZoneText = dropZoneMsg;

// variables for drag-and-drop
let draggables, start, end;


//
// OPTIONS
//
let showOptionsMenu = false
let autoplay = true;
let repeat = false;
let repeatCount = 1;
let title_max_length = 30;


//
// FUNCTIONS
//

// Get the names of the selected files and return as an array
function getFileNames(array) {
  file_names = [];
  for (let i = 0; i < array.length; i++) {
    file_names.push(array[i].name);
  }
  return file_names;
}

// Update the playlist whenever there is a change
function updateMusicList(file_names) {
  while(musicList.firstChild) {
    musicList.removeChild(musicList.lastChild);
  }
  file_names.forEach(element => {
    const listItem = document.createElement('li');

    // Testing - drag and drop
    listItem.setAttribute('draggable', true);
    listItem.classList.add('draggable');
    

    listItem.textContent = element.split('.')[0];
    if (listItem.textContent.length > title_max_length) {
        listItem.textContent = listItem.textContent.slice(0,title_max_length-3) + "...";
    }
    if (element === files[current_track_number].name) {
      listItem.classList.add("active");
    }
    musicList.appendChild(listItem);
  });


    draggables = musicList.querySelectorAll('.draggable')  // drag and drop test
    refreshDragAndDrop(draggables);
}

// Plays the selected track
function player(track_number) {
  let file = files[track_number];
  music.src = URL.createObjectURL(file);
  title = file.name.split(".")[0];
  track_name = file.name;
  if (title.length > title_max_length) {
      title = title.slice(0, title_max_length-3) + "...";
  }
  musicTitle.forEach((item) => {item.innerText = title});
  music.play();
  //document.title = title;
  updateMusicList(file_names);
  updatePlayPause();
}

// Play / Pause
function playPause() {
  if (music.paused) {
    music.play();
  } else {
    music.pause();
  }
  updatePlayPause();
}

// Update play/pause status in buttons
function updatePlayPause() {
    if (music.paused) {
        for (let i = 0; i < playButtons.length; i++) {
            playButtons[i].classList.remove("hide");
        }
        for (let i = 0; i < pauseButtons.length; i++) {
            pauseButtons[i].classList.add("hide");
        }
    } else {
        for (let i = 0; i < playButtons.length; i++) {
            playButtons[i].classList.add("hide");
        }
        for (let i = 0; i < pauseButtons.length; i++) {
            pauseButtons[i].classList.remove("hide");
        }
    }
}

// Play Next Track
function next() {
  if (current_track_number < total_count) {
    current_track_number += 1;
    player(current_track_number);
  }
}

// Play Previous Track
function prev() {
  if (current_track_number > 0) {
    current_track_number -= 1;
    player(current_track_number);
  } else {
    music.currentTime = 0;
  }
}

// Load the selected files and start player
function go() {
  file_names = getFileNames(files);
  total_count = files.length - 1;
  updateMusicList(file_names);
  player(current_track_number);
  if (files.length > 0) {
    filesPage.classList.add("hide");
    mainPage.classList.remove("hide");
    controlsPage.classList.remove("hide");
  }
}

// only loads audio files and discards others
function removeOtherTypes(files) {
    const Re = new RegExp('audio');
    let audioFiles = []
    for (let i = 0; i < files.length; i++) {
        if (Re.test(files[i].type)) {
            audioFiles.push(files[i]);
        }
    }
    return audioFiles;
}

// Fisher-Yates shuffle algorithm
function shuffle(array) {
  var j, x, i;
  for (i = array.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = array[i];
    array[i] = array[j];
    array[j] = x;
  }
  return array;
}

// uses shuffle function to shuffle the tracks
function shuffleTracks() {
    // not an elegant solution, but I don't care.
    files = shuffle(files);
    file_names = getFileNames(files);
    current_track_number = file_names.indexOf(track_name);
    let temp = files[0];
    files[0] = files[current_track_number];
    files[current_track_number] = temp;
    file_names = getFileNames(files);
    current_track_number = 0;
    updateMusicList(file_names);
}

// reload function that can be activated by the user
function reload() {
    music.pause();
    filesPage.classList.remove("hide");
    mainPage.classList.add("hide");
    controlsPage.classList.add("hide");
    optionsToggle.classList.remove('options-selected');
    optionsContainer.classList.add('invisible');
}

// tied to updateMusicList(file_names) function and starting point for drag and drop function
function refreshDragAndDrop(draggables) {
    draggables.forEach(draggable => {
        draggable.addEventListener('dragstart', () => {
            draggable.classList.add('dragging');
        })
        draggable.addEventListener('dragend', () => {
            draggable.classList.remove('dragging');
        })
    })
}

// gets the nearest element for drag and drop. (I don't know how it works. So don't touch it!)
function getDragAfterElement(y) {
    draggableElements = [...musicList.querySelectorAll('.draggable:not(.dragging)')]
    return draggableElements.reduce((closest, child) => {
        box = child.getBoundingClientRect()
        offset = y - box.top - box.height / 2
        if (offset < 0 && offset > closest.offset) {
            return { offset: offset, element: child }
        } else {
            return closest
        }
    }, { offset: Number.NEGATIVE_INFINITY }).element
}

//
// EVENT LISTENERS
//

// automatically load when there is a change in file browser
fileBrowser.addEventListener("change", () => {
    files = removeOtherTypes(fileBrowser.files);
    go();
});

// needed for drag and drop of files (files drag and drop will not work otherwise)
dropZone.addEventListener('dragover', (e) => {
    e.preventDefault();
})

// automatically load when files are dropped by the user
dropZone.addEventListener('drop', (e) => {
    e.preventDefault();
    files = removeOtherTypes(e.dataTransfer.files);
    dropZoneMsg.innerText = `${files.length} uploaded`;
    go();
})

// change the drop zone message when the files are already loaded. (usually hidden and not seen by the user)
if (fileBrowser.files.length > 0) {
    files = removeOtherTypes(fileBrowser.files);
    dropZoneMsg.innerText = `${files.length} uploaded`;
}

// Decides what to do when the music ends
music.addEventListener("ended", () => {
  if (repeat) {
    if (repeatCount === -1 || repeatCount > 0) {
      if (repeatCount > 0) {
        repeatCount -= 1;
        repeatCounterDisplay.innerText = repeatCount;
      }
      music.currentTime = 0;
      music.play();
    } else {
      repeat = false;
      toggleRepeat.classList.remove("toggled-on");
      repeatCounter.classList.add("invisible");
      next();
    }
  } else if (autoplay) {
    next();
  } else {
    music.pause();
    updatePlayPause();
  }
});

// Plays the song selected from the playlist
musicList.addEventListener("click", (e) => {
  let clickedItem = e.target;
  if (clickedItem.tagName === "LI") {
    current_track_number = Array.from(clickedItem.parentElement.children).indexOf(clickedItem);
    player(current_track_number);
    updateMusicList(file_names);
  }
});

// for animations
optionsToggle.addEventListener("click", () => {
    showOptionsMenu = !showOptionsMenu;
    if (showOptionsMenu) {
        optionsToggle.classList.add('options-selected');
        optionsContainer.classList.remove('invisible');
    } else {
        optionsToggle.classList.remove('options-selected');
        optionsContainer.classList.add('invisible');
    }
})
toggleAutoplay.addEventListener("click", () => {
    autoplay = !autoplay;
    if (autoplay) {
        toggleAutoplay.classList.add("toggled-on");
    } else {
        toggleAutoplay.classList.remove("toggled-on");
    }
})
toggleRepeat.addEventListener("click", () => {
    repeat = !repeat;
    if (repeat) {
        toggleRepeat.classList.add("toggled-on");
        repeatCounter.classList.remove("invisible");
    } else {
        toggleRepeat.classList.remove("toggled-on");
        repeatCounter.classList.add("invisible");
    }
})
repeatInc.addEventListener("click", () => {
    if (repeatCount < 9) {
        repeatCount += 1;
    }
    repeatCounterDisplay.innerText = repeatCount;
})
repeatDec.addEventListener("click", () => {
    if (repeatCount > -1) {
        repeatCount -= 1;
    }
    repeatCounterDisplay.innerText = repeatCount;
})

// for the progress bar to update periodically (1000 = 1 second)
window.setInterval(() => {
  progressValue = music.currentTime / music.duration * 100 ;
  if (progressValue <= 100 || progressValue >= 0) {
    progressBar.value = progressValue;
  } else {
    progressBar.value = 0;
  }
  document.documentElement.style.setProperty('--progress', progressValue+'%');
}, 1000);

// jump to the user specified time (works in touch screens also)
progressBar.addEventListener('input', () => {
    music.currentTime = progressBar.value * music.duration / 100;
})

// For Track Drag and Drop (Don't change it. It is very hard to fix if it breaks)
musicList.addEventListener("dragover", (e) => {
    e.preventDefault()
    afterElement = getDragAfterElement(e.clientY)
    draggable = document.querySelector('.dragging')

    if (afterElement == null) {
        musicList.appendChild(draggable)
    } else {
        musicList.insertBefore(draggable, afterElement)
    }
})
musicList.addEventListener("dragstart", (e) => {
    x = e.target
    start = Array.from(x.parentElement.children).indexOf(x)
})
musicList.addEventListener("dragend", (e) => {
    y = e.target
    end = Array.from(y.parentElement.children).indexOf(y)

    temp = files.splice(start, 1)
    files = files.slice(0,end).concat(temp).concat(files.slice(end))
    file_names = getFileNames(files)
    
    // changing current track number according to the changed list
    if (start === current_track_number) {
        current_track_number = end;
    } else if (start < current_track_number && end >= current_track_number) {
        current_track_number -= 1;
    } else if (start > current_track_number && end <= current_track_number) {
        current_track_number += 1;
    }

    updateMusicList(file_names)
})


// Keyboard Keybindings
window.addEventListener('keydown', (e) => {
    if (e.key === "n" || e.key === "N") { next(); }
    if (e.key === "p" || e.key === "P") { prev(); }
    if (e.key === "k" || e.key === "K") { playPause(); }
    if (e.key === "j" || e.key === "J") { music.currentTime -= 15; }
    if (e.key === "l" || e.key === "L") { music.currentTime += 30; }
    if (e.key === "r" || e.key === "R") { go(); }
})
