//
// VARIABLES
//
const goButton = document.getElementById('go-button');
const musicTitle = document.getElementById('music-title');
const fileBrowser = document.getElementById('file-browser');
const musicList = document.getElementById('music-list');
const music = document.getElementById('music-player');
const autoplay = document.getElementById('autoplay');
const repeat = document.getElementById('repeat');
const repeatCounter = document.getElementById('repeat-counter');

let files = [];
let total_count = 0;
let current_track_number = 0;
let title = "";


//
// OPTIONS
//
music.controls = true;
autoplay.checked = true;
repeat.checked = false;


//
// FUNCTIONS
//

// Get the names of the selected files and return as an array
function getFiles() {
  files = fileBrowser.files;
  file_names = [];
  for (let i = 0; i < files.length; i++) {
    file_names.push(files[i].name);
  }
  return file_names;
}

// Update the playlist whenever there is a change
function updateMusicList(files) {
  while(musicList.firstChild) {
    musicList.removeChild(musicList.lastChild);
  }
  files.forEach(element => {
    const listItem = document.createElement('li');
    listItem.textContent = element.split('.')[0];
    if (element === fileBrowser.files[current_track_number].name) {
      listItem.classList.add("active");
    }
    musicList.appendChild(listItem);
  });
}

// Plays the selected track
function player(track_number) {
  let file = fileBrowser.files[track_number];
  music.src = URL.createObjectURL(file);
  title = file.name.split(".")[0];
  musicTitle.innerText = title;
  music.play();
  //document.title = title;
  updateMusicList(files);
}

// Play Next Track
function next() {
  if (current_track_number < total_count) {
    current_track_number += 1;
    player(current_track_number);
  }
}

// Play Previous Track
function prev() {
  if (current_track_number > 0) {
    current_track_number -= 1;
    player(current_track_number);
  } else {
    music.currentTime = 0;
  }
}

// Load the selected files and start player
function go() {
  files = getFiles();
  total_count = files.length - 1;
  updateMusicList(files);
  player(current_track_number);
}


//
// EVENT LISTENERS
//

// Automatically starts player after selecting the files
fileBrowser.addEventListener("change", go);

// Decides what to do when the music ends
music.addEventListener("ended", () => {
  if (repeat.checked) {
    if (repeatCounter.value === "-1" || repeatCounter.value > 0) {
      if (repeatCounter.value > 0) {
        repeatCounter.value -= 1;
      }
      music.currentTime = 0;
      music.play();
    } else {
      repeatCounter.classList.add("hide");
      repeat.checked = false;
      next();
    }
  } else if (autoplay.checked) {
    next();
  } else {
    music.pause();
  }
});

// Handles the Repeat function
repeat.addEventListener("change", () => {
  if (repeat.checked) {
    repeatCounter.value = 1;
    repeatCounter.classList.remove("hide");
  } else {
    repeatCounter.classList.add("hide");
    repeatCounter.value = 0;
  }
});

// Plays the song selected from the playlist
musicList.addEventListener("click", (e) => {
  let clickedItem = e.target;
  if (clickedItem.tagName === "LI") {
    current_track_number = Array.from(clickedItem.parentElement.children).indexOf(clickedItem);
    player(current_track_number);
    updateMusicList(files);
  }
});

// Keyboard Keybindings
window.addEventListener('keydown', (e) => {
    if (e.key === "PageDown") {
        next();
    }
    if (e.key === "PageUp") {
        prev();
    }
})
